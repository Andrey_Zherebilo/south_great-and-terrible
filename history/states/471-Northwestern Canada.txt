
state={
	id=471
	name="STATE_471" # Northwestern Canada
	manpower = 20000	
	state_category = wasteland
	impassable = yes	

	history={
		owner = NSA
		buildings = {
			infrastructure = 1
		}
		add_core_of = NSA
	}

	provinces={
		917 1872 4678 12508 5403
	}
}
