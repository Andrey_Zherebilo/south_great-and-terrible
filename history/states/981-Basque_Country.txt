
state={
	id=981
	name="STATE_981"
	manpower = 891710
	
	state_category = city
	
	resources={
		steel=8 # was: 12
	}

	history={
		owner = SPA
		victory_points = {
			740 3 
		}
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			740 = {
				naval_base = 1
			}
		}
		add_core_of = SPA
	}

	provinces={
		740 3737 3933 6756
	}
}
