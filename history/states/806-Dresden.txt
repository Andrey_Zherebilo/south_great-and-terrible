
state={
	id=806
	name="STATE_806"
	manpower = 2000000
	resources={
		steel=10
	}

	state_category = city


	history={
		owner = GCM
		buildings = {
			infrastructure = 7
			arms_factory = 1
			industrial_complex = 2
		}
		victory_points = {
			514 5 
		}
		add_core_of = GCM
		add_core_of = GRS
		1939.1.1 = {
			buildings = {
				synthetic_refinery = 2
				arms_factory = 2
				industrial_complex = 4
			}
		}
	}

	provinces={
		514 573 3514 6441 13778 13872
	}
}
