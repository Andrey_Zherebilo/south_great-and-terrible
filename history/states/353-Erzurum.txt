
state={
	id=353
	name="STATE_353"
	manpower = 523537
	state_category = town

	history={
		owner = TUR
		buildings = {
			infrastructure = 3
		}
		add_core_of = TUR
		add_core_of = KRD
	}

	provinces={
		652 876 4583 7470 9858 10399 11880 10497 12318  13121
	}
}
