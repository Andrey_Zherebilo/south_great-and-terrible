state={
	id=937
	name="STATE_937"
	manpower = 200000
	
	state_category = town

	history={
		owner = PCM
		victory_points = {
			13597 1 # Cieszyn
		}
		buildings = {
			infrastructure = 6
		}
		add_core_of = PCM
	}

	provinces={
		9412 13591 13597
	}
}
