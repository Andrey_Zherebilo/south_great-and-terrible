
state={
	id=841
	name="STATE_841"
	manpower = 900000
	
	state_category = town

	history={
		owner = GCM
		victory_points = {
			3673 1
		}
		buildings = {
			infrastructure = 5
		}
		add_core_of = GCM

		1938.3.12 = {
			owner = GCM
			controller = GCM
			add_core_of = GCM
		}
	}

	provinces={
		668 6673 3673 11612
	}
}
