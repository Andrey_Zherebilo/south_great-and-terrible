
state={
	id=1055
	name="STATE_1055" #Palestine
	manpower = 51082
	
	state_category = wasteland

	history={
		owner = BPL
		buildings = {
			infrastructure = 2
		}
		victory_points = {
			4088 1 
		}
		add_core_of = PAL
		add_core_of = ISR
		add_core_of = BPL
	}

	provinces={
		1015 7176 11970
	}
}
