
state={
	id=803
	name="STATE_803"
	manpower = 340000
	
	state_category = town

	history={
		owner = GCM 
		victory_points = {
			6282 1
		}
		
		buildings = {
			infrastructure = 6
			13791 = {
				naval_base = 4
			}
		}
		add_core_of = GCM
		add_core_of = GRS
	}

	provinces={
		6282 13791
	}
}
