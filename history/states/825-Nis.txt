
state={
	id=825
	name="STATE_825"
	manpower = 200000
	
	state_category = rural
	
	resources={
		chromium=10
		aluminium=5
	}

	history={
		owner = BUL
		buildings = {
			infrastructure = 4
		}
		victory_points = {
			11868 1
		}
		add_core_of = BUL
		add_core_of = SER
	}

	provinces={
		11857 11868 
	}
}
