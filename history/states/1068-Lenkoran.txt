state={
	id=1068
	name="STATE_1068"
	manpower = 408000
	resources={
		oil=5 # was: 126.000
	}

	history={
		owner = BAX
		victory_points = {
			7661 1 
		}
		buildings = {
			infrastructure = 2
			industrial_complex = 1

		}
		add_core_of = KAW
		add_core_of = BAX

	}

	provinces={
		 4473 12434 
	}
	manpower=1651049
	buildings_max_level_factor=1.000
	state_category=large_town
}