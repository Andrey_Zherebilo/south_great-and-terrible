
state={
	id=1044
	name="STATE_1044"
	manpower = 122510
	
	state_category = large_town

	history={
		owner = SPA
		buildings = {
			infrastructure = 6
			industrial_complex = 1
		}
		add_core_of = SPA
	}

	provinces={
		903 6936 9872 11684  
	}
}
