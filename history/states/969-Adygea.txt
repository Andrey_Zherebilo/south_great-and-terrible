
state={
	id=969
	name="STATE_969"
	manpower = 126500
	
	state_category = city

	history={
		owner = RCM 
		victory_points = {
			3720 1 
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 1
		}
		add_core_of = RCM 
		
	}

	provinces={
		3717 3720 6721 6738
	}
}
