
state={
	id=152
	name="STATE_152"
	manpower = 2021759
	
	state_category = town
	
	resources={
		steel=3
		aluminium=1 # was: 1
	}

	history={
		owner = GCM
		victory_points = {
			6723 5 
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 2
		}
		add_core_of = 

		1938.3.12 = {
			owner = GCM
			controller = GCM
			add_core_of = 
		}
	}

	provinces={
		732 3684 3703 6708 6723 9665 
	}
}
