
state={
	id=103
	name="STATE_103" # Dalmatia
	manpower = 781700

	state_category = large_town


	history={
		owner = ITA
		buildings = {
			infrastructure = 5
			industrial_complex = 1
		}
		add_core_of = ITA
		#add_claim_by = ILL #added by focus
	}

	provinces={
		591 984 3601 6611 11564
	}
}
