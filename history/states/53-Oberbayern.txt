
state={
	id=53
	name="STATE_53"
	manpower = 1453365
	
	state_category = city

	history={
		owner = GCM
		victory_points = { 3299 5 }
		buildings = {
			infrastructure = 6
		}
		add_core_of = GCM
		add_core_of = GRS
	}

	provinces={
		532 571 586 3299 3541 3571 6725 9515 9681 11497 
	}
}
