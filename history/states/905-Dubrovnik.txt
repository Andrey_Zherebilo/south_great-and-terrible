
state={
	id=905
	name="STATE_905" # Dalmatia
	manpower = 100000

	state_category = rural


	history={
		owner = ITA
		victory_points = {
			3868 1 
		}
		buildings = {
			infrastructure = 5
			6889 = {
				naval_base = 1
			}
		}
		add_core_of = ITA
	}

	provinces={
		3868 6889 11816 13323 13389 13912
	}
}
