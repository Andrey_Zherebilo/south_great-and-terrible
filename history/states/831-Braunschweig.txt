
state={
	id=831
	name="STATE_831"
	manpower = 700000
	resources={
		oil=2
	}
	
	state_category = city

	history={
		owner = GRS
		buildings = {
			infrastructure = 7

		}
		add_core_of = GRS
		add_core_of = GCM

		1939.1.1 = {
			buildings = {
				industrial_complex = 1
			}
		}
	}

	provinces={
		526 9375 6218 9264 6263 6298 6325
	}
}
