state= {
	id=1067
	name="STATE_1067"
	manpower = 408000
	resources={
		chromium=45 # was: 84
		steel = 10 # was: 20
	}
	
	state_category = large_town

	history= {
		owner = GRE
		buildings = {
			infrastructure = 5
		}
		add_core_of = TUR
		add_core_of = GRE
	}
	provinces = { 
		922 11842 3893
	}
}
