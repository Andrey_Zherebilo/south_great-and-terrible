
state={
	id=805
	name="STATE_805"
	manpower = 1000000
	buildings_max_level_factor = 1

	state_category = town


	history={
		owner = GRS
		victory_points = { 3367 1 }
		buildings = {
			infrastructure = 8
			arms_factory = 2
			industrial_complex = 1
			anti_air_building = 2

		}
		add_core_of = GRS
		add_core_of = GCM

		1939.1.1 = {
			buildings = {
				air_base = 10
				anti_air_building = 5
				arms_factory = 4
				industrial_complex = 2
			}
		}
	}

	provinces={
		375 3367 11219
	}
}
