state={
	id=9
	name="STATE_9"
	manpower = 2492600
	resources={
		steel=3 # was: 8
	}
	
	state_category = large_city
	
	history={
		owner = GCM
		victory_points = { 11542 20 }
		victory_points = { 583 1 }
		buildings = {
			infrastructure = 7
			arms_factory = 3
			industrial_complex = 1
			anti_air_building = 5
			air_base = 5
		}
		add_core_of = GCM

		1939.3.14 = {
			owner = GCM
			controller = GCM
		}
	}

	provinces={
		445 494 583 3462 6440 9414 9429 11542 13777 13871
	}
}
