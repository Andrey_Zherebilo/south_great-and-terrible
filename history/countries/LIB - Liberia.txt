capital = 298

oob = "LIB_1936"

set_technology = {
	infantry_weapons = 1
	gwtank = 1
	basic_light_tank = 1
}

set_convoys = 20

1939.1.1 = {

	add_political_power = 1198
	
	#generic focuses
	complete_national_focus = army_effort
	complete_national_focus = equipment_effort
	complete_national_focus = motorization_effort
	complete_national_focus = aviation_effort
	complete_national_focus = construction_effort_2
	complete_national_focus = production_effort_2
	complete_national_focus = infrastructure_effort
	complete_national_focus = industrial_effort
	complete_national_focus = construction_effort
	complete_national_focus = production_effort
	
	oob = "LIB_1939"
	set_technology = {
		early_fighter = 1
		CAS1 = 1
		gw_artillery = 1

		#doctrines
		air_superiority = 1
		grand_battle_plan = 1
		trench_warfare = 1
		fleet_in_being = 1
		battlefleet_concentration = 1
		convoy_sailing = 1


		#electronics
		electronic_mechanical_engineering = 1
		radio = 1
		radio_detection = 1
		mechanical_computing = 1

		#industry
		basic_machine_tools = 1
		improved_machine_tools = 1
		advanced_machine_tools = 1
		synth_oil_experiments = 1
		oil_processing = 1
		improved_oil_processing = 1
		fuel_silos = 1
		construction1 = 1
		construction2 = 1
		dispersed_industry = 1
		dispersed_industry2 = 1
	}
	
}


set_popularities = {
	fascism = 0
	communism = 0
	democratic = 0
	neutrality = 0
	monarchy = 100
}

set_politics = {
	ruling_party = monarchy
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}


create_country_leader = {
	name = "William Tubman"
	desc = "_DESC"
	picture = "gfx/leaders/LIB/William_Tubman.dds"
	expire = "1965.1.1"
	ideology = despotism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Daniel Howard"
	desc = "_DESC"
	picture = "gfx/leaders/LIB/Daniel_Howard.dds"
	expire = "1965.1.1"
	ideology = marxism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Edwin Barclay"
	desc = "_DESC"
	picture = "gfx/leaders/LIB/Edwin_Barclay.dds"
	expire = "1965.1.1"
	ideology = fascism_ideology
	traits = {
		#
	}
}

create_country_leader = {
	name = "Louis Grimes"
	desc = "_DESC"
	picture = "gfx/leaders/LIB/Louis_Grimes.dds"
	expire = "1965.1.1"
	ideology = socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Charles King"
	desc = "_DESC"
	picture = "gfx/leaders/LIB/Charles_King.dds"
	expire = "1965.1.1"
	ideology = absolutly_monarchy
	traits = {
		#
	}
}

1939.1.1 = {
	set_popularities = {
	fascism = 0
	communism = 0
	democratic = 0
	neutrality = 0
	monarchy = 100
}
		
	set_politics = {
		ruling_party = monarchy
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
	}
}